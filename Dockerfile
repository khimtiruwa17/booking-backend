FROM node:10
# WORKDIR ./src/server
# WORKDIR /backend/src/server;
WORKDIR /backend/

COPY package.json ./src/server
RUN npm install
# COPY  /backend 
CMD ['npm',"start"]
