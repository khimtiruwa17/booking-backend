require('dotenv').config();
var mongoose = require("mongoose");
module.exports.app = {
    port: process.env.PORT || 300,
    jwtSecret: process.env.JWT_KEY,
    useremail: process.env.email,
    password: process.env.password,
    socketPort: process.env.socketPort
}
const dbURL = "mongodb://127.0.0.1:27017/newapi"
// const dbURL = process.env.dbURL;
console.log(`db connect is `, dbURL);
mongoose.connect(dbURL, {
    useNewUrlParser: true
});
mongoose.connection.once("open", function () {
    console.log("Database connection success");
});
mongoose.connection.on("error", function (error) {
    console.log("Connection Failure");
    console.log("Error in connecting to Database");
});