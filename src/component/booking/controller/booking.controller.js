const bookingQuery = require('./../query/booking.query');
function getBookingInfo(req, res, next) {
    condition = {
        // user: req.loggedInUser._id
    }
    bookingQuery.getBooking(condition)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });

}
function newBooking(req, res, next) {
    var body = req.body;
    bookingQuery.insert(body)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}
function update(req, res, next) {
    var body = req.body;
    var origin = req.headers.origin
    console.log(`origin is`, origin);
    originurl = origin + 'booking/list'
    // console.log(`origin url form angular is `, req.headers)
    if (req.headers.referer === originurl) {
        if (req.body.status === 'confirm')
            body.status = 'confirmed'
        if (req.body.status === 'reject')
            body.status = 'reject'
    }
    bookingQuery.BookingUpdate(req.params.id, body)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

function remove(req, res, next) {
    bookingQuery.CancelRemove(req.params.id)
        .then((response) => {
            res.status(200).json(
                {
                    response,
                    message: " booking cancel "
                }
            );
        })
        .catch((err) => {
            next(err);
        });
}

module.exports = {
    newBooking,
    getBookingInfo,
    update,
    remove,
}

