var bookingModel = require('./../models/booking.models')
// bookingDetails = data
function addDays(date, days) {
    if (date === null) {
        date = new Date()
        console.log(`and date is`, date)
    }
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};
function map_booking(booking, data) {
    if (data.firstname)
        booking.firstname = data.firstname;
    if (data.lastname)
        booking.lastname = data.lastname;
    if (data.contactNumber)
        booking.contactNumber = data.contactNumber;
    if (data.email)
        booking.email = data.email;
    if (data.city)
        booking.city = data.city;
    if (data.status)
        booking.status = data.status;
    // console.log(`booking query map booking`, booking);
    return booking;
}
/**
  * @param {get my booking} condition 
 */
function getBooking(condition) {
    // console.log(' booking query condition ', condition);
    return new Promise(function (resolve, reject) {
        bookingModel.find(condition)
            .exec(function (err, data) {
                if (err) {
                    reject(err)
                }
                if (data.length) {
                    resolve(data)
                }
                else {
                    resolve({
                        message: 'No booking yet',
                        status: 204
                    })
                }
            })
    });
}
/**
 * 
 * @param {new boooking data} data 
 */
function insert(data) {
    return new Promise(function (resolve, reject) {
        // var id = data.id
        var Booking = new bookingModel({});
        var newBooking = map_booking(Booking, data)
        newBooking.save(function (err, saved) {
            if (err) {
                reject(err)
            }
            else {
                resolve(saved)
            }
        })

    });
}
function BookingUpdate(id, data) {
    // confirm booking by adminstar and makes changes to 
    return new Promise(function (resolve, reject) {
        bookingModel.findById(id, function (err, booking) {
            if (err) {
                reject(err);
            }
            if (booking) {
                var updateMappedbooking = map_booking(booking, data);
                // updateMappedbooking.status = 'confirm'
                // console.log(`update data is`, data)
                updateMappedbooking.save(function (err, done) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(done);
                    }
                });
            } else {
                reject({
                    message: 'booking not found',
                    status: 404
                });
            }
        });
    });
}
/**
 * 
 * @param {booking ref object id  } id 
 */
function CancelRemove(id) {
    console.log(`booking id is `, id)
    return new Promise(function (resolve, reject) {
        bookingModel.findByIdAndRemove(id, function (error, done) {
            if (error) {
                reject(error);
            } else {
                resolve(done)
            }
        })
    });
}
module.exports = {
    insert,
    getBooking,
    BookingUpdate,
    CancelRemove,
}

