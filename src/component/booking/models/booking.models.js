var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bookingSchema = new Schema({
    // user: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'user'
    // },
    // room: {
    //     type: Schema.Types.ObjectId,
    //     unique: true,
    //     required: true,
    //     sparse: true,
    //     ref: 'product'
    // },
    // checkIn: {
    //     type: Date,
    // },
    // checkOut: {
    //     type: Date
    // },
    // noOfdays: Number,
    firstname: String,
    lastname: String,
    email: String,
    contactNumber: Number,
    city: String,
    status: String,
    checkIn: Date,
    checkOut: Date,
    room: {
        type: Schema.Types.ObjectId,
        unique: true,
        required: true,
        sparse: true,
        ref: 'product'
    },

},
    {
        timestamps: true
    });

var bookingModel = mongoose.model("booking", bookingSchema);
module.exports = bookingModel;