
var express = require("express");
var router = express.Router();
var multer = require('multer');
var bookingCtrl = require('./../controller/booking.controller');
var BoookingModel = require('./../models/booking.models')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images/profile');
        console.log(`hello insinde multer`)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
});
var upload = multer({
    storage: storage
});
router.route('/')
    .get(bookingCtrl.getBookingInfo)
    .post(bookingCtrl.newBooking)
// router.route('confirm/:id')
router.route("/:id")
    .put(bookingCtrl.update)
    .delete(bookingCtrl.remove)
module.exports = router;