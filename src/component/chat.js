var socket = require('socket.io');
var users = [];
var uniqueUser = [];
// var uniqueFruits = []

function uniqBy(a, key) {
    var seen = {};
    return a.filter(function (item) {
        var k = key(item);
        return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    })
}

function uniq_fast(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for (var i = 0; i < len; i++) {
        var item = a[i];
        if (seen[item] !== 1) {
            seen[item] = 1;
            out[j++] = item;
        }
    }
    return out;
}
module.exports = function (app, port) {
    // var socketIo = socket(app.listen(port));
    const server = app
        .listen(port, function (error, done) {
            if (error) {
                console.log("server listening failed");
            } else {
                console.log("server listening at port ", port);
                console.log("press CTRL+C to stop from server");
            }
        });

    var socketIo = socket(server);
    socketIo.on('connection', function (client) {
        // console.log('client connected to socket server at port ', con.app.socketPort);
        var id = client.id;
        client.on('user', function (user) {
            console.log('conneted user is ', user);
            // console.log('conneted client is  ', client);
            users.push({
                name: user,
                id: id
            });
            // console.log(`users are`, users);
            client.emit('users', users);
            client.broadcast.emit('users', users)
            client.emit("hello", 'response from server');
        })
        client.on('new-msg', function (msg) {
            // console.log('message in new-msg', msg);
            // client.emit('reply-msg', msg);
            client.emit('own-msg', msg);
            console.log('message in new-msg or own messages is', msg);

            // client.broadcast.emit('reply-msg', msg);
            client.broadcast.to(msg.receiver).emit('reply-msg', msg);
        })
        client.on('typing', function (data) {
            client.broadcast.to(data.receiver).emit('typing', data);
        });
        client.on('typing-stop', function (data) {
            client.broadcast.to(data.receiver).emit('typing-stop', data);
        });
        // client.on('disconnect', function (client) {
        client.on('disconnect', function () {
            // console.log('disconnected client', client);
            users.forEach(function (item, index) {
                if (item.id == id) {
                    users.splice(index, 1);
                }
                client.emit('users', users);
                client.broadcast.emit('users', users);
            });
        })
    });
    return socketIo;
}