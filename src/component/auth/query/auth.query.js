var passwordHash = require("password-hash");
var randomString = require('randomstring');
const UserModel = require('../../shared/model/user.model');

const UserMap = require('./../usermapping');
/**
  * @param {user obj} user 
 */
function login(user) {
    return new Promise(function (resolve, reject) {
        console.log(`user data are in login query auth is`, user)
        UserModel.findOne(user).exec(function (error, user) {
            if (error) {
                reject({
                    error: error,
                    message: 'user not found'
                });
            } else {
                resolve(user);
            }
        });
    });
}
// };
/**
  * @param {object with user data in req.body} data 
 */
function registerUser(data) {
    return new Promise(function (resolve, reject) {
        var newUser = UserModel({});
        var newMappedUser = UserMap.map_user_req(newUser, data.body);
        newMappedUser.save(function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    });
}
function forgotPassword(data) {
    // console.log('auth query user inside forgot password is:', data);
    return new Promise(function (resolve, reject) {
        UserModel.findOne({ email: data })
            .exec(function (error, user) {
                console.log('auth query user inside forgot password is ::', user);
                if (error) {
                    reject({
                        error: error,
                        message: 'email not found'
                    });
                }
                else {
                    if (user) {
                        var token = randomString.generate(10);
                        var oneDay = new Date().getTime() + 1000 * 60 * 60 * 24;
                        // var twoDaysLater = new Date().getTime() + 1000 * 60 * 60 * 24 * 2;

                        user.passwordResetToken = token;
                        user.passwordResetTokenExpiry = new Date(oneDay);
                        user.save(function (error, done) {
                            resolve(user);
                            if (error) {
                                return next(error);
                            }
                        });
                        // }

                    }
                }

            });
    });
};
function reset(token) {
    // console.log('user data for reset is :', data)
    return new Promise(function (resolve, reject) {
        UserModel.findOne({ passwordResetToken: token })
            .exec(function (error, user) {
                if (error) {
                    reject({
                        error: error,
                        message: 'token not found || user with such token not found'
                    });
                } else {
                    resolve(user);
                }
            });
    });
};

module.exports = {
    userlogin: login,
    register: registerUser,
    forgotPassword: forgotPassword,
    resetPassword: reset
}
// };
