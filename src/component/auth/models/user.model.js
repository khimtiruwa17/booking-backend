var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var addressSchema = new Schema({
    // phone: {
    //   type: Number
    // },
    // mobile: {
    //   type: Number
    // },
    permanentAddress: {
        type: String
    },
    temporaryAddress: {
        type: String
    }
})
var userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        // unique: true,

    },
    lastname: {
        type: String,
        // unique: true,

    },
    phonenumber: {
        type: String,
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    role: {
        type: Number,
        enum: [1, 2, 3], //1 for admin, 2 end user, 3 visitor
        default: 2
    },
    image: String,
    contact: addressSchema,
    passwordResetToken: String,
    passwordResetTokenExpiry: Date

}, {
        timestamps: true
    });
var model = mongoose.model("user", userSchema);
module.exports = model;