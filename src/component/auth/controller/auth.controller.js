var jwt = require('jsonwebtoken');
var configuration = require('./../../../config');
var sender = require('./../../nodemail');
const AuthQuery = require('./../query/auth.query');
var passwordHash = require("password-hash");

function createToken(data) {
    var token = jwt.sign({
        id: data._id,
        role: data.role,
        username: data.username
    }, configuration.app.jwtSecret);
    return token;
}

function prepareMail(options) {
    var mailOptions = {
        from: "EcoSharing System",
        to: options.email,
        subject: options.subject,
        text: options.text,
        html: options.html
    };
    return mailOptions;
}

/**
 * this is get product method
 * @param {object} req 
 * @param {object} res 
 * @param {method} next 
 * @returns Boolean
 */
function login(req, res, next) {
    var condition = {};
    var Email = req.body.username.includes('@');
    console.log(`email is ${Email}`);
    if (Email) {
        Email = req.body.username;
    }
    condition = {
        $or: [{
            "username": req.body.username
        }, {
            "email": Email
        }]

    };
    AuthQuery.userlogin(condition)
        .then((userlogined) => {
            if (userlogined) {
                var match = passwordHash.verify(req.body.password, userlogined.password);
                if (match) {
                    var token = createToken(userlogined);
                    res.status(200).json({
                        user: userlogined,
                        token: token,
                        message: 'sucessfully logged In'
                    });
                } else {
                    next({
                        message: 'incorrect password',
                        status: 400
                    })
                }
            } else {
                console.log(' incorrect username', req.body.username);
                next({
                    message: 'incorrect username',
                    status: 404
                })
            }
        })
        .catch((error) => {
            next({
                message: error + 'user not found', //"error in getting user from db  ",
                status: 404
            });
        });
};
/**
 * @param {user req from form } req 
 * @param {server res to user} res 
 * @param {methods with arguments for forward erros to error handling middleware} next 
 */
function register(req, res, next) {
    AuthQuery.register(req)
        .then((response) => {
            res.status(200).json(response);
            var Email = req.body.username.includes('@');
            console.log(`email is ${Email}`);
            res.status(200).json(done);
        })
        .catch((err) => {
            next(err);
        });
};


function forgotPassword(req, res, next) {
    AuthQuery.forgotPassword(req.body.email)
        .then((userlogined) => {
            var link = req.headers.origin + '/reset-password/' + userlogined.passwordResetToken;
            var username = userlogined.username;
            subject = "hello mail ✔";
            html = `<div> Dear <b> ${username}</b></div>
                     <p>Sincerely yours,</p>
                     <p><b>EcoSharing Team</b></p>
                   </div>`;
            text = "test mail"
            var mailData = prepareMail({
                email: req.body.email,
                // name: username,
                subject: subject,
                // link: link,
                text: text,
                html: html,
            });
            sender.transporter.verify((err, success) => {
                if (err) console.error(err);
                console.log('Your config is correct');
                // console.log(`sucess in sender verify in email sucess is `, success)
            });
            sender.transporter.sendMail(mailData, function (error, done) {
                if (error) {
                    console.log(`error in sender.sendmail is are`, error)
                    return next(error);
                }
                console.log(`done or sucess in  in sender.sendmail is are`, done)
                res.status(200).json(done);
            });
        })
        .catch((error) => {
            next({
                error: error.error,
                message: "error in getting user email from db  ",
                status: 404
            });
        });
};

function resetPassword(req, res, next) {
    var token = req.params.token;
    AuthQuery.resetPassword(token)
        .then(
            (userResetPassword) => {
                if (userResetPassword) {
                    if (new Date(userResetPassword.passwordResetTokenExpiry).getTime() < new Date().getTime()) {
                        return next({
                            message: 'Reset password token expired',
                            status: 400
                        });
                    }
                    userResetPassword.password = passwordHash.generate(req.body.password);
                    userResetPassword.passwordResetToken = null;
                    userResetPassword.passwordResetTokenExpiry = null;
                    userResetPassword.save(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.status(200).json({
                            done: done,
                            message: 'password changed sucessfully'
                        });
                    });
                } else {
                    next({
                        message: "token mismatched",
                        status: 400
                    })
                }
            })
        .catch(
            (error) => {
                next({
                    error: error,
                    message: 'inside catch block reset password'
                }

                );
            }
        )
};

module.exports = {
    login: login,
    reg: register,
    forgotPassword: forgotPassword,
    reset: resetPassword,

}