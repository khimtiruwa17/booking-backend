var express = require("express");
var router = express.Router();
var authControl = require('./../controller/auth.controller')

router.route("/login")
    .post(authControl.login);
router.route('/register')
    .post(authControl.reg)
router.route("/forgot-password")
    .post(authControl.forgotPassword)
router.route('/reset-password/:token')
    .post(authControl.reset)
module.exports = router;
