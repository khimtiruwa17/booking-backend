var nodemailer = require('nodemailer');
var config = require('./../config');
// const user = `khimtiruwa17@gmail.com`
var auth = {
    user: config.app.useremail,
    pass: config.app.password,
}

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: auth,
});

module.exports = {
    transporter
}