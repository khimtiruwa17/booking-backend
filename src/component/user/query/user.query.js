var userModel = require('./../../shared/model/user.model');

function map_user_req(user, userdata) {
    // if (userdata.firstName && userdata.lastName)
    //   user.name = userdata.firstName + " " + userdata.lastName;
    if (userdata.firstname) user.firstname = userdata.firstname;
    if (userdata.lastname) user.lastname = userdata.lastname;
    if (userdata.username) user.username = userdata.username;
    if (userdata.password) {
        //user.password = userdata.password;
        // user.password = passwordHash.generate(req.body.password);
        user.password = passwordHash.generate(userdata.password);
    }
    if (userdata.dob) //user.dob = userdata.dob;
        user.dob = new Date(userdata.dob);
    if (userdata.role) user.role = userdata.role;
    if (userdata.email) user.email = userdata.email;
    if (userdata.gender) user.gender = userdata.gender;
    if (userdata.phonenumber) user.phonenumber = userdata.phonenumber;
    if (userdata.image) user.image = userdata.image;
    if (userdata.city) user.city = userdata.city;
    if (userdata.state) user.state = userdata.state;

    return user;
}

function getUser(condition) {
    return new Promise(function (resolve, reject) {
        userModel.find(condition).exec(function (err, done) {
            if (err) {
                reject(err)
            }
            else {
                // console.log('done data are', done)
                resolve(done)
            }
        })
    });
}
// function 
function update(id, data) {
    return new Promise(function (resolve, reject) {
        userModel.findById(id, function (err, user) {
            if (err) {
                reject(err);
            }
            if (user) {
                var updateMappedUser = map_user_req(user, data);
                updateMappedUser.save(function (err, done) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(done);
                    }
                });
            } else {
                reject({
                    message: 'error',
                    status: 404
                });
            }
        });
    });
}

function remove(id) {
    return new Promise(function (resolve, reject) {
        userModel.findByIdAndRemove(id, function (error, done) {
            if (error) {
                reject(error);
            } else {
                resolve(done)
            }
        })
    });
}
module.exports = {
    getUser,
    update,
    remove,
}

