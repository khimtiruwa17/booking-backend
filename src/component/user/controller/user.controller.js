const userQuery = require('./../query/user.query');

function getUser(req, res, next) {
    var condition = {};
    userQuery.getUser(condition)
        .then((response) => {
            console.log(`reponse data are`, response);
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}
function getUserById(req, res, next) {
    var condition = {
        _id: req.params.id
    }
    userQuery.getUser(condition)
        .then((response) => {
            // console.log(`reponse data are`, response);
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });

}


function update(req, res, next) {
    var body = req.body;
    if (req.file) {
        body.image = req.file.filename;
    }
    // console.log(`incommming data is`, body.email)
    userQuery.update(req.params.id, body)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}
function remove(req, res, next) {
    userQuery.remove(req.params.id)
        .then((response) => {
            res.status(200).json(
                {
                    response,
                    message: " user deleted"
                }
            );
        })
        .catch((err) => {
            next(err);
        });
}

module.exports = {
    getUser,
    getUserById,
    update,
    remove,
}

