
var express = require("express");
var router = express.Router();
// var multer = require('multer');
var userController = require('./../controller/user.controller');
var uploading = require('./../../shared/storage');

// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, './files/images/profile');
//         console.log(`hello insinde multer`)
//     },
//     filename: function (req, file, cb) {
//         cb(null, Date.now() + '-' + file.originalname)
//     }
// });
// var upload = multer({
//     storage: storage
// });
router.route("/")
    .get(userController.getUser)
// .post(upload.single('img'), productCtrl.insert);
router.route("/:id")
    .get(userController.getUserById)
    .put(uploading.upload.single('img'), userController.update)
    .delete(userController.remove)
module.exports = router;