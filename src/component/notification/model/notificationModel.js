var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var NotificationSchema = new Schema({
  device_id: {
    type: Schema.Types.ObjectId,
    ref: 'Device'
  },
  client_id: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  message: String,
  seen: {
    type: Boolean,
    default: false
  },
  link: String,
  type: String
}, {
  timestamps: true
});

module.exports = mongoose.model('Notification', NotificationSchema);