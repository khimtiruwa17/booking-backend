var sharp = require('sharp')
const fs = require('fs-extra');
module.exports = function (req, res, next) {
    if (req.file) {
        //configure sharp
        let width = 1920;
        let height = 1080;
        // console.log(`hello khim sharp`, (req.file))
        var file = `./files/images/product/img` + '-' + Date.now() + '-' + req.file.originalname
        appendfilename = 'img' + '-' + Date.now() + '-' + req.file.originalname
        req.file.name = appendfilename
        // console.log(`inside sharp append file image name is `, appendfilename)
        sharp(req.file.path)
            .rotate()
            .resize(width, height)
            .toFile(file, function (err) {
                if (!err) {
                    // console.log('file uploadeded sucessfully')
                    const path = './files/images/product/' + req.file.originalname
                    fs.access(path, fs.F_OK, (err) => {
                        if (err) {
                            // console.error(err)
                            // console.log(`no such file `)
                            return
                        }
                        fs.unlink(path, function (error, done) {
                            if (error) {
                                // console.log(`inside sharp to revome original file`)
                                next(error)
                            }
                            // console.log(`original file deleted`)
                        });
                    })
                    next();
                }
                else {
                    // console.log(err)
                    next(err)
                }
            })
            .png()
    }
    next();
};
