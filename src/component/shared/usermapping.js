var passwordHash = require("password-hash");
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
// map usering
// obj1 :user
// obj2 : userDetails

function encrypt(text) {
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

// var hw = encrypt("Some serious stuff")
// console.log(hw)
// console.log(decrypt(hw))



function map_user_req(user, userDetails) {
    console.log('user data is :::::', user);
    console.log('user details data is :::', userDetails);
    // if (userDetails.firstName && userDetails.lastName)
    //   user.name = userDetails.firstName + " " + userDetails.lastName;
    if (userDetails.firstname) user.firstname = userDetails.firstname;
    if (userDetails.lastname) user.firstname = userDetails.lastname;
    if (userDetails.username) user.username = userDetails.username;
    if (userDetails.password) {
        user.password = passwordHash.generate(userDetails.password);
        // user.password = encrypt(userDetails.password)
        // user.password = crypto.createHmac('sha256', (userDetails.password)
        //     .update('I love cupcakes')
        //     .digest('hex')

    }
    if (userDetails.role) user.role = userDetails.role;
    if (userDetails.email) user.email = userDetails.email;
    return user;
}
// module.exports = map_user_req
module.exports = {
    map_user_req, encrypt, decrypt
}
