
function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.tags)
        product.tags = productDetails.tags.split(', ');
    if (productDetails.image)
        product.image = productDetails.image;
    if (productDetails.for)
        product.for = productDetails.for;
    if (productDetails.address)
        product.address = productDetails.address;
    return product;
}



module.exports = map_product_req;