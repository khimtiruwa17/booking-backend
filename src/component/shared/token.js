var jwt = require('jsonwebtoken');
var config = require('./../../configuration');

function createToken(data) {
    console.log('data is::', data)
    var token = jwt.sign({
        id: data._id,
        username: data.username,
        role: data.role
    }, config.app.jwtSecret);
    return token;
}
module.exports = createToken();