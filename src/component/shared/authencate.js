var jwt = require('jsonwebtoken');
var config = require('./../../config');
UserModel = require('./model/user.model');
var passwordHash = require("password-hash");

module.exports = function(req, res, next) {
    var token;
     // var match = passwordHash.verify(token);
    // var varify;
    // var match = passwordHash.verify(req.body.password, userlogined.password);
    /**
     * token request from header || query || 
     * x-acess-token and authorization are used for andriod mostly 
     */
    // console.log( 'request is ', req)
    // token = req.headers.x-access-token;
    // console.log(`token is ${req.headers.token}`);
    // console.log(`req data area ${req}`);
    if (req.headers.token) {
        token = req.headers.token;
    }
    if (req.query.token) {
        token = req.query.token
    }
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization']
    }
    /**
     * token requet 
     * after we get token 
     * process token 
     * either it vaild or not
     * decoded is value used to create token
     */
    // console.log("token is :", token);
    // res.json({ token: token })
      // console.log(`if before condition token is`s, token)
    if (token) {
         // token = passwordHash.verify(token,token);
         console.log(`if condition token is`, token)
        jwt.verify(token, config.app.jwtSecret, function(error, decoded) {
            if (error) {
                return next(error);
            }
            // console.log('decoded value is ', decoded);
            UserModel.findById(decoded.id)
                .then(function(user) {
                    if (user) {
                        req.loggedInUser = user;
                        user.password = null;
                        user.passwordResetToken = null;
                        user.passwordResetTokenExpiry = null;
                        return next();
                    } else {
                        next({
                            message: 'User deleted form system',
                            status: 204
                        })
                    }
                })
                .catch(function(err) {
                    next(err);
                })
        });
    } else {
        return next({
            message: "Token Not Found",
            status: 400
        });
    }
};