var multer = require('multer');
const fs = require('fs-extra');

//directory
const dirProduct = './files/images/product/';
const dirProfile = './files/images/profile/';
// const dirApp = './files/images/app/';
// const dirVideo = './files/video/';
fs.ensureDirSync(dirProduct);
fs.ensureDirSync(dirProfile);
// fs.ensureDirSync(dirApp);
// fs.ensureDirSync(dirVideo);
//end of dir//
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images/profile/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
var uploaduploadProfile = multer({
    storage: storage
});
var addProduct = multer({
    storage:
        multer.diskStorage({
            destination: function (req, file, cb) {
                console.log(`i am inside add product multer and file is `, file)
                cb(null, './files/images/product/')
            },
            filename: function (req, file, cb) {
                cb(null, file.originalname)
                console.log(`i am inside add product multer and file is `, 'sucessfully')
            }
        })
})
var addVedio = multer({
    storage:
        multer.diskStorage({
            destination: function (req, file, cb) {
                // console.log(`i am inside add product multer and file is `, file)
                cb(null, './files/video/')
            },
            filename: function (req, file, cb) {
                cb(null, Date() + '_' + file.originalname)

            }
        })
})
module.exports = {
    upload: uploaduploadProfile,
    uploadproduct: addProduct,
    uploadVedio: addVedio
}


//it's okay not to know everything