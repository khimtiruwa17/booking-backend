var express = require("express");
var router = express.Router();
// var multer = require('multer');
var productCtrl = require('./../controllers/product.controller');
var uploading = require('./../../shared/storage');
var size = require('./../../shared/resize');
var authenticate = require('./../../shared/authencate');

router.route("/")
    .get(productCtrl.getProduct)
    .post(uploading.uploadproduct.single('img'), authenticate, size, productCtrl.insertProduct);
router.route("/:id")
    .get(productCtrl.getProductById)
    .put(authenticate, uploading.uploadproduct.single('img'), size, productCtrl.update)
    .delete(authenticate, productCtrl.remove)
module.exports = router;