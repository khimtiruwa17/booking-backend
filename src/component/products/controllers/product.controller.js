// var socket = require('socket.io');
// var socketService = require('./../../chat')
const productQuery = require("./../query/product.query");

/**
 * this is get product method
 * @param {object} req 
 * @param {object} res 
 * @param {method} next 
 * @returns Boolean
 */
function getProduct(req, res, next) {
    var condition = {};
    productQuery.find(condition)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

function getProductById(req, res, next) {
    var condition = {
        _id: req.params.id
    }
    productQuery.findbyId(condition)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

function insertProduct(req, res, next) {
    var body = req.body;
    console.log(`body data are`, req.body)
    body.user = req.loggedInUser._id;
    if (req.file) {
        body.image = req.file.name;
    }
    productQuery.insert(body)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

function update(req, res, next) {
    var body = req.body;
    if (req.file) {
        body.image = req.file.name;
    }
    productQuery.update(req.params.id, body)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

function remove(req, res, next) {
    productQuery.remove(req.params.id)
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            next(err);
        });
}

module.exports = {
    getProduct,
    getProductById,
    insertProduct,
    update,
    remove,

}

