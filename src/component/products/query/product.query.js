const ProductModel = require("./../models/product.model");
const fs = require('fs-extra');
function map_product_req(product, productDetails) {

    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.tags)
        product.tags = productDetails.tags.split(', ');
    if (productDetails.image)
        product.image = productDetails.image;
    if (productDetails.for)
        product.for = productDetails.for;
    if (productDetails.type)
        product.type = productDetails.type;
    return product;
}
function find(condition) {
    return new Promise(function (resolve, reject) {
        ProductModel.find(condition)
            .populate('user', {
                password: 0,
                passwordResetToken: 0,
                passwordResetTokenExpiry: 0,
            })
            .exec(function (err, done) {
                if (err) {
                    reject(err);
                } else {
                    resolve(done)
                }
            });
    });
}
function findbyId(condition) {
    return new Promise(function (resolve, reject) {
        ProductModel.findOne(condition)
            .populate('user', {
                password: 0,
                passwordResetToken: 0,
                passwordResetTokenExpiry: 0,
            })
            .exec(function (err, done) {
                if (err) {
                    reject(err);
                } else {
                    resolve(done)
                }
            });
    });
}

// findbyId(condition){
//     return new Promise(function (resolve, reject) {
//         ProductModel.findOne(
//             condition
//         )
//             .populate('user', {
//                 password: 0,
//                 passwordResetToken: 0,
//                 passwordResetTokenExpiry: 0,
//             })
//             .exec(function (err, done) {
//                 if (err) {
//                     reject(err);
//                 } else {
//                     resolve(done)
//                 }
//             });
//     });
// }

//////////////////////

/**
 * it will insert new product to db
 * @param {Object} data 
 * @returns Promise
 */
function insert(data) {
    console.log(`saivng data is `, data)
    return new Promise(function (resolve, reject) {
        var newProduct = new ProductModel();
        var mappedProduct = map_product_req(newProduct, data);
        mappedProduct.vedio = data.vedio
        mappedProduct.image = data.image;
        mappedProduct.user = data.user;
        mappedProduct.save(function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    });
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                reject(err);
            }
            if (product) {
                var updateMappedProduct = map_product_req(product, data);
                // console.log(`update data is`, data)
                updateMappedProduct.save(function (err, done) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(done);
                    }
                });
            } else {
                reject({
                    message: 'Product not found',
                    status: 404
                });
            }
        });
    });
}

function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findByIdAndRemove(id, function (error, done, next) {
            if (error) {
                reject(error);
            } else {
                if (done.image) {
                    const path = './files/images/product/' + done.image
                    fs.access(path, fs.F_OK, (err) => {
                        if (err) {
                            console.error(err)
                            console.log(`no such file `)
                            return next(err)

                        }
                        fs.unlink('./files/images/product/' + done.image, function (error, done) {
                            if (error) {
                                next(error)
                                console.log(`no such file `)
                            }
                        });
                    })
                    resolve(done)
                }

                else {
                    console.log('no file found');
                    resolve(done);
                }
            }
        })
    });
}
module.exports = {
    find,
    findbyId,
    insert,
    update,
    remove,
}

