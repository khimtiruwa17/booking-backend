var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    description: {
        type: String
    },
    price: {
        type: Number
    },
    type: {
        type: String
    },
    status: {
        type: String,
        enum: ['availabe', 'Booked', 'Not-Booked', 'booked'],
        default: 'availabe'
    },
    capacity: {
        type: Number,
    },
    for: {
        type: String,
    },
    tags: [String],
    image: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
});

var productModel = mongoose.model("product", productSchema);
module.exports = productModel;