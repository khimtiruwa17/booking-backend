var socket = require('socket.io');
var users = [];
var config = require('./../../config');
// module.exports = function (app, port) {
//     // var socketIo = socket(app.listen(port));
//     // const server = app
//     //     .listen(port, function (error, done) {
//     //         if (error) {
//     //             console.log("server listening failed");
//     //         } else {
//     //             console.log("server listening at port ", port);
//     //             console.log("press CTRL+C to stop from server");
//     //         }
//     //     });

//     var socketIo = socket(server);
//     socketIo.on('connection', function (client) {
//         // console.log('client connected to socket server at port ', con.app.socketPort);
//         var id = client.id;
//         client.on('user', function (user) {
//             console.log('conneted user is ', user);
//             // console.log('conneted client is  ', client);
//             users.push({
//                 name: user,
//                 id: id
//             });
//             console.log(`users are`, users);
//             client.emit('users', users);
//             client.broadcast.emit('users', users)
//             client.emit("hello", 'response from server');
//         })
//         client.on('new-msg', function (msg) {
//             console.log('message in new-msg', msg);
//             // client.emit('reply-msg', msg);
//             client.emit('own-msg', msg);
//             // client.broadcast.emit('reply-msg', msg);
//             client.broadcast.to(msg.receiver).emit('reply-msg', msg);
//         })
//         client.on('typing', function (data) {
//             client.broadcast.to(data.receiver).emit('typing', data);
//         });
//         client.on('typing-stop', function (data) {
//             client.broadcast.to(data.receiver).emit('typing-stop', data);
//         });
//         // client.on('disconnect', function (client) {
//         client.on('disconnect', function () {
//             // console.log('disconnected client', client);
//             users.forEach(function (item, index) {
//                 if (item.id == id) {
//                     users.splice(index, 1);
//                 }
//                 client.emit('users', users);
//                 client.broadcast.emit('users', users);
//             });
//         })
//         client.on('product', function (data) {
//             console.log(`hello product event`);
//             // client.broadcast.to(data.receiver).emit('typing-stop', data);
//         });
//     });
//     return socketIo;
// }

module.exports = function (app) {
    var socket = require('socket.io');
    var io = socket(app.listen(config.app.socketPort))
    console.log('socket server listening at port', config.app.socketPort)
    io.on('connection', function (client) {
        console.log('client connected to server');
        var socketId = client.id;
        client.on('new-user', function (data) {
            users.push({
                id: socketId,
                name: data
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        client.on('new-msg', function (data) {
            //  intresting part
            console.log('msg at BE >>', data);
            client.emit("reply-msg-own", data); // this will emit event to requested client only
            client.broadcast.to(data.receiverId).emit('reply-msg', data); // this will emit for every conected client execpt own 
        })
        client.on('typing', function (data) {
            client.broadcast.to(data.receiverId).emit('is-typing');
        })
        client.on('typing-stop', function (data) {
            client.broadcast.to(data.receiverId).emit('is-typing-stop');
        })
        client.on('disconnect', function (data) {
            console.log('disconnected data >>', data);
            users.forEach(function (item, i) {
                if (item.id == socketId) {
                    users.splice(i, 1);
                }
            });
            client.broadcast.emit('users', users);
        })

    });
}

