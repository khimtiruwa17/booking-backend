const express = require("express");
const app = express();
const morgan = require("morgan");
const path = require("path");
const bodyParser = require("body-parser");
var con = require("./config");

// console.log("config data are", con)

var cors = require('cors');
// importing routes
var authRoute = require('./component/auth/routes/auth.routes');
var userRoute = require('./component/user/routes/user.routes');

var productRoute = require('./component/products/routes/product.route');
var bookingRoute = require('./component/booking/routes/booking.routes');
var authenticate = require('./component/shared/authencate');
app.use(cors());
app.use(morgan('dev'));
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(bodyParser.json());
app.use('/img', express.static('files'));
app.use("/auth", authRoute);
// app.use('/notification', userRoute);
// app.use("/product", authenticate, productRoute);
app.use("/product", productRoute);
app.use("/booking", bookingRoute);


//application error handling
app.use(function (req, res, next) {
    next({
        message: 'Not Found',
        status: 404
    })
})
// error handling middleware
app.use(function (error, req, res, next) {
    res.status(error.status || 400);
    res.json({
        status: error.status,
        message: error.message
    });
});
// socket stuff
// require('./socket')(app);
require('./component/socket/chat')(app)
app.listen(con.app.port, function (error, done) {
    if (error) {
        console.log("server listening failed");
    } else {
        console.log("server listening at port ", con.app.port);
        console.log("press CTRL+C to exit from server");
    }
});
